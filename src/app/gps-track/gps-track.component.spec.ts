import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpsTrackComponent } from './gps-track.component';

describe('GpsTrackComponent', () => {
  let component: GpsTrackComponent;
  let fixture: ComponentFixture<GpsTrackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsTrackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsTrackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
