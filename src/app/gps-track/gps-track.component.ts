import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import Ws from '@adonisjs/websocket-client';
import * as L from 'leaflet';


@Component({
  selector: 'app-gps-track',
  templateUrl: './gps-track.component.html',
  styleUrls: ['./gps-track.component.css']
})
export class GpsTrackComponent implements OnInit {
  map: any;
  ws: any;
  imeis = new FormControl();
  imeiList: any = [
  {viewValue: 9728, value: 357454075121887 },
  {viewValue: 4807, value: 352093081151051},
  {viewValue: 6334, value: 357454075120749},
  {viewValue: 9852, value: 356917056656064},
  {viewValue: 9201, value: 356917057934932},
  {viewValue: 7242, value: 352093081638719}];

  isConnected: boolean;

  constructor() {
    this.ws = Ws('ws://iswm-dev.acceldash.com');
    this.ws.connect();

    this.ws.on('open', () => {
      this.isConnected = true;
    });

    this.ws.on('close', () => {
      this.isConnected = false;
    });

  }

  ngOnInit() {
    this.map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  subscribeToTopic(imei) {
    this.ws.close();

    this.ws.on('close', () => {
      this.subscribeCall(imei);
    });
  }

  subscribeCall(imei) {
    this.ws = Ws('ws://iswm-dev.acceldash.com');
    this.ws.connect();
    this.ws.on('open', () => {
      this.isConnected = true;
      /* subscribing to a topic */
      const topic = this.ws.subscribe('gps_data:' + imei.value);

      topic.on('message', (response) => {
          L.marker([response.lat, response.lng]).addTo(this.map)
          .bindPopup('speed:' + response.speed + '<br>' + 'time:' + response.server_datetime)
          .openPopup();
      });

      topic.on('error', (error) => {
        console.log(error);
      });
    });
  }

}
